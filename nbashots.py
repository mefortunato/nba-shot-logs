from json import JSONDecodeError
import requests
import numpy as np
import pandas as pd

TEAM_ID_INFO = None

baseurl = 'https://stats.nba.com/stats/'

headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-AU,en-US;q=0.9,en-GB;q=0.8,en;q=0.7',
    'Connection': 'keep-alive',
    'DNT': '1',
    'Host': 'stats.nba.com',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (X11; CrOS x86_64 11647.154.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.114 Safari/537.36',
    'referer': 'http://stats.nba.com/scores'
}

global_params = {
    'leagueId': '00', 'season': '2015-16', 'seasonType': 'Regular Season',
    'outcome': '', 'location': '', 'month': 0, 'seasonSegment': '',
    'dateFrom': '', 'dateTo': '', 'opponentTeamId': 0,
    'vsConference': '', 'vsDivision': '', 'position': '',
    'playerPosition': '', 'rookieYear': '', 'gameSegment': '', 'period': 0,
    'lastNGames': 0, 'clutchTime': '', 'aheadBehind': '', 'pointDiff': '',
    'rangeType': 0, 'startPeriod': 1, 'endPeriod': 10,
    'startRange': 0, 'endRange': 2147483647,
    'contextFilter': '', 'contextMeasure': 'FGA'
}

def get_team_id_info():
    resp = requests.get(baseurl+'franchisehistory/?leagueId=00', headers=headers)
    resp_json = resp.json()
    results = resp_json['resultSets'][0]
    return pd.DataFrame(results['rowSet'], columns=results['headers'])

def get_team_ids(season):
    global TEAM_ID_INFO
    if TEAM_ID_INFO is None:
        TEAM_ID_INFO = get_team_id_info()
    season = season.split('-')[0]
    return TEAM_ID_INFO.loc[(TEAM_ID_INFO['END_YEAR']>=season)&(TEAM_ID_INFO['START_YEAR']<=season), 'TEAM_ID'].unique()

def get_player_ids(season, team_id):
    params = {
        'leagueId': '00',
        'season': season,
        'teamId': team_id
    }
    resp = requests.get(baseurl+'commonteamroster/', headers=headers, params=params)
    resp_json = resp.json()
    results = resp_json['resultSets'][0]
    player_ids = np.array([row[-1] for row in results['rowSet']])
    return player_ids

def get_player_game_shots(season, team_id, player_id, game_id=''):
    '''
    season: 2015-16
    teamId: 1610612745
    playerId: 201935
    gameId: 0021501207
    '''
    req_params = dict(**global_params)
    req_params['season'] = season
    req_params['teamId'] = team_id
    req_params['playerId'] = player_id
    req_params['gameId'] = game_id
    resp = requests.get(baseurl+'shotchartdetail/', headers=headers, params=req_params)
    try:
        resp_json = resp.json()
    except JSONDecodeError:
        print(resp.text)
        raise JSONDecodeError
    df = pd.DataFrame(resp_json['resultSets'][0]['rowSet'], columns=resp_json['resultSets'][0]['headers'])
    return df

if __name__ == '__main__':
    seasons = ['{}-{:02}'.format(s, s-1999) for s in np.arange(2000, 2019)[::-1]]
    df = []
    for season in seasons:
        teams = nbashots.get_team_ids(season)
        for team in teams:
            players = nbashots.get_player_ids(season=season, team_id=team)
            for player in players:
                dfs.append(nbashots.get_player_game_shots(season='', team_id=team, player_id=player))
        df = pd.concat(df).reset_index(drop=True)
        df.to_csv('shots.{}.csv.gz'.format(season), compression='gzip')
